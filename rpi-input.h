#ifndef RPI_INPUT_H
#define RPI_INPUT_H

#include <stdint.h>

typedef enum 
{
    INPUT_KEYUP = 0x100,
    INPUT_KEYDOWN = 0x101,
    INPUT_KEYREPEAT = 0x102,
    INPUT_MOUSEBUTTONUP = 0x200,
    INPUT_MOUSEBUTTONDOWN = 0x201,
    INPUT_MOUSEMOTION = 0x300
} InputEventType;

typedef struct
{
    uint16_t type;
    uint16_t code; // button and keycodes from linux/input.h
    int32_t motion[3]; // X, Y, mouse_wheel
} InputEvent;

int InputInit(void);
void InputClose(void);
int InputPollEvent(InputEvent* pEvent);
int InputIsKeyDown(uint16_t keyCode);
int InputIsButtonDown(uint16_t buttonCode);

#endif
