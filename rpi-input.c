#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <regex.h>
#include <dirent.h>
#include <fcntl.h>
#include <linux/input.h>

#include <termios.h>
 
#include <rpi-input.h>

#define TRUE 1
#define FALSE 0

#define INPUT_BUFFER_SIZE 64

struct EventBuffer
{
    struct input_event buffer[INPUT_BUFFER_SIZE];
    int first;
    int count;
};

static struct EventBuffer keyboardEventBuffer;
static struct EventBuffer mouseEventBuffer;

static int keyboardFd = -1;
static int mouseFd = -1;
static int mouseMove[3];

#define INPUT_NUM_KEYS 256
#define INPUT_NUM_BUTTONS 32

static int keyStates[INPUT_NUM_KEYS];
static int buttonStates[INPUT_NUM_BUTTONS];

static struct termios stdinTermios;

int TermiosInit(void) {
    if (tcgetattr(fileno(stdin), &stdinTermios) != 0) {
        return -1;
    }

    stdinTermios.c_lflag &= ~ECHO;

    if (tcsetattr(fileno(stdin), TCSAFLUSH, &stdinTermios) != 0) {
        return -1;
    }
}

void TermiosRestore(void) {
    stdinTermios.c_lflag |= ECHO;
    tcsetattr(fileno(stdin), TCSAFLUSH, &stdinTermios);
}

int InputInit(void)
{
    keyboardEventBuffer.first = keyboardEventBuffer.count = 0;
    mouseEventBuffer.first = mouseEventBuffer.count = 0;
    keyboardFd = mouseFd = -1;
    mouseMove[0] = mouseMove[1] = mouseMove[2] = 0;

    regex_t kbd,mouse;
    if(regcomp(&kbd,"event-kbd",0)!=0)
    {
        printf("regcomp for kbd failed\n");
        return FALSE;
    }
    if(regcomp(&mouse,"event-mouse",0)!=0)
    {
        printf("regcomp for mouse failed\n");
        return FALSE;
    }

    DIR *dirp;
    const char *dirName = "/dev/input/by-id";
    if ((dirp = opendir(dirName)) == NULL) {
        perror("couldn't open '/dev/input/by-id'");
        return FALSE;
    }

    // Find any files in the directory that match the regex for keyboard or mouse
    struct dirent *dp;
    do {     
        errno = 0;
        
        if ((dp = readdir(dirp)) != NULL) 
        {
            //printf("readdir (%s)\n",dp->d_name);

            char fullPath[1024];
            int result;

            if(regexec (&kbd, dp->d_name, 0, NULL, 0) == 0)
            {
                //printf("match for the kbd = %s\n",dp->d_name);
                sprintf(fullPath,"%s/%s",dirName,dp->d_name);
                keyboardFd = open(fullPath,O_RDONLY | O_NONBLOCK);
                //printf("%s Fd = %d\n",fullPath,keyboardFd);
                //printf("Getting exclusive access: ");
#ifdef RPI_INPUT_EXCLUSIVE_KEYBOARD
                result = ioctl(keyboardFd, EVIOCGRAB, 1);
                if (result != 0)
                    printf("Couldn't grab exclusive keyboard access\n");
#endif
            }
            if(regexec (&mouse, dp->d_name, 0, NULL, 0) == 0)
            {
                //printf("match for the kbd = %s\n",dp->d_name);
                sprintf(fullPath,"%s/%s",dirName,dp->d_name);
                mouseFd = open(fullPath,O_RDONLY | O_NONBLOCK);
                //printf("%s Fd = %d\n",fullPath,mouseFd);
                //printf("Getting exclusive access: ");
                result = ioctl(mouseFd, EVIOCGRAB, 1);
                if (result != 0)
                    printf("Couldn't grab exclusive mouse access\n");
            }

        }
    } while (dp != NULL);

    closedir(dirp);

    regfree(&kbd);
    regfree(&mouse);

    if (keyboardFd == -1)
    {
        printf("couldn't find keyboard event file\n");
        InputClose();
        return FALSE;
    }

    if (mouseFd == -1)
    {
        printf("couldn't find mouse event file\n");
        InputClose();
        return FALSE;
    }

    // Save old termios settings
    if (TermiosInit() == -1) {
        InputClose();
    }
    
    // Restore termios settings
    if (atexit(TermiosRestore) != 0) {
        TermiosRestore();
        InputClose();
    }

    return TRUE;
}

void InputClose(void)
{
    int result;

    if (mouseFd != -1)
    {
        //printf("Releasing exclusive mouse access: ");
        result = ioctl(mouseFd, EVIOCGRAB, 0);
        if (result != 0)
            printf("Couldn't release exclusive mouse access\n");
        close(mouseFd);
    }

    if (keyboardFd != -1)
    {
#ifdef RPI_INPUT_EXCLUSIVE_KEYBOARD        
        //printf("Releasing exclusive keyboard access: ");
        result = ioctl(keyboardFd, EVIOCGRAB, 0);
        if (result != 0)
            printf("Couldn't release exclusive keyboard access\n");
#endif
        close(keyboardFd);
    }
}

int InputPollEvent(InputEvent* pEvent)
{
    // Read more events from keyboard if the buffer is empty
    if (keyboardEventBuffer.count == 0 && keyboardFd != -1)
    {
        int bytesRead = read(keyboardFd, keyboardEventBuffer.buffer, sizeof(struct input_event)*INPUT_BUFFER_SIZE);
        if (bytesRead > 0)
        {

            keyboardEventBuffer.first = 0;
            keyboardEventBuffer.count = bytesRead / sizeof(struct input_event);
        }
    }

    // Read more events from mouse if the buffer is empty
    if (mouseEventBuffer.count == 0 && mouseFd != -1)
    {
        int bytesRead = read(mouseFd, mouseEventBuffer.buffer, sizeof(struct input_event)*INPUT_BUFFER_SIZE);
        if (bytesRead > 0)
        {
            mouseEventBuffer.first = 0;
            mouseEventBuffer.count = bytesRead / sizeof(struct input_event);
        }
    }

    // Process events until finding one that should be reported
    while (TRUE)
    {
        if (keyboardEventBuffer.count == 0 && mouseEventBuffer.count == 0)
            return FALSE;

        // Find the oldest event from either buffer
        struct EventBuffer* pInput;
        if (keyboardEventBuffer.count == 0)
            pInput = &mouseEventBuffer;
        else
        {
            if (mouseEventBuffer.count == 0)
                pInput = &keyboardEventBuffer;
            else 
            {
                struct timeval* keyboardEventTime = &keyboardEventBuffer.buffer[keyboardEventBuffer.first].time;
                struct timeval* mouseEventTime = &mouseEventBuffer.buffer[mouseEventBuffer.first].time;

                if ((keyboardEventTime->tv_sec < mouseEventTime->tv_sec) ||
                    ((keyboardEventTime->tv_sec == mouseEventTime->tv_sec) && (keyboardEventTime->tv_usec < mouseEventTime->tv_usec)))
                    pInput = &keyboardEventBuffer;
                else
                    pInput = &mouseEventBuffer;
            }        
        }
        
        // Get the event from the buffer
        struct input_event* evp = &pInput->buffer[pInput->first];
        pInput->first++;
        pInput->count--;

        // relative mouse movement
        if(evp->type == EV_REL)
        {
            if(evp->code == REL_X)
                mouseMove[0] = evp->value;
            
            else if(evp->code == REL_Y)
                mouseMove[1] = evp->value;              

            else if(evp->code == REL_WHEEL)
                mouseMove[2] = evp->value;             
        }

        // sync: end of a logical mouse action
        else if(evp->type == EV_SYN)
        {
            if (pInput == &mouseEventBuffer && (mouseMove[0] || mouseMove[1] || mouseMove[2]))
            {
                int i;
                
                pEvent->type = INPUT_MOUSEMOTION;
                for (i=0; i<3; i++)
                    pEvent->motion[i] = mouseMove[i];
                mouseMove[0] = mouseMove[1] = mouseMove[2] = 0;
                return TRUE;
            }
        }

        // mouse button or keypress
        else if(evp->type == EV_KEY)
        {
            if (pInput == &mouseEventBuffer)
            {
                pEvent->type = INPUT_MOUSEBUTTONUP + evp->value;
                pEvent->code = evp->code;

                uint16_t index = evp->code - BTN_MISC;
                if (index < INPUT_NUM_BUTTONS)
                    buttonStates[index] = (evp->value != 0);
            }
            else
            {
                pEvent->type = INPUT_KEYUP + evp->value;
                pEvent->code = evp->code;

                uint16_t index = evp->code;
                if (index < INPUT_NUM_KEYS)
                    keyStates[index] = (evp->value != 0);
            }
            return TRUE;
        }
    }
}

int InputIsKeyDown(uint16_t keyCode)
{
    return keyStates[keyCode];
}

int InputIsButtonDown(uint16_t buttonCode)
{
    return buttonStates[buttonCode - BTN_MISC];
}
